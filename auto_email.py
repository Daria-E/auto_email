'''
This assumes a dir structure of:
current_dir
    auto_email.py
    recepients.txt --> recepient1\nrecepient2\nrecepient3...
    reports
        currentdate
            recepient1.pdf
            recepient2.pdf
            ...
        currentdate - 1
        ...
    logs
        currentdate
        currentdate - 1
'''

import smtplib, os, email, schedule, datetime

def compose_email(recepient):
    msg = email.message.Message()
    subject = f"Your daily report from OurGreatCompany ({datetime.date.today()})"
    msg['Subject'] = subject
    msg['From'] = "DONOTREPLY@OurGreatCompany.com"
    msg['To'] = recepient
    msg['Body'] = "Dear customer,\n Please find attached your daily report.\n Regards,\n OurGreatCompany"

    with open(f'reports/{datetime.date.today()}/{recepient}', 'rb') as content_file:
        content = content_file.read()
        msg.add_attachment(content, maintype='application', subtype='pdf', filename='example.pdf')
    
    return(msg)

def send_email(message, server):
    try:
        server.send_message(message)
    except:
        with open (f'logs'/{datetime.date.today()}, 'rb') as log:
            log.append(f"Message failed:\n Recepient: {message['To']}")

def main():
    user = "<your gmail here>"
    pw = "<your app password here>"
    host = "<your hostname here>"
    port = 000

    recepients = os.open("recepients.txt")
    report_dir = f'reports/{datetime.date.today()}'

    server = smtplib.SMTP_SSL(host, port) #smtp credentials
    server.login(user, gmail_pass)

    for recepient in recepients:
        send_email(compose_email(recepient), server)
    
    server.quit()

if __name__ == "__main__":
	main()